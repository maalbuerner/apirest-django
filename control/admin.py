from django.contrib import admin
from control.models import Asset, Owner, Category 

admin.site.register(Asset)
admin.site.register(Owner)
admin.site.register(Category)
