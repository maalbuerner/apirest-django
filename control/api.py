from .serializers import AssetSerializer, OwnerSerializer, CategorySerializer
from .models import Asset, Owner, Category
from rest_framework import generics

class AssetList(generics.ListCreateAPIView):
	queryset = Asset.objects.all()
	serializer_class = AssetSerializer

class OwnerList(generics.ListCreateAPIView):
	queryset = Owner.objects.all()
	serializer_class = OwnerSerializer

class CategoryList(generics.ListCreateAPIView):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer

class AssetDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Asset.objects.all()
	serializer_class = AssetSerializer

class OwnerDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Owner.objects.all()
	serializer_class = OwnerSerializer

class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer	