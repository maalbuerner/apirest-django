#encoding=utf8
from django.db import models

STATUS_CHOICES = [
	('miss', 'Miss'),
	('readed', 'Readed'),
	('none', 'None'),
]

class Owner(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	def __str__(self):
		return self.name

class Category(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=255)
	def __str__(self):
		return self.name

class Asset(models.Model):
	id = models.AutoField(primary_key=True)	
	price = models.FloatField(default=0)
	epc_id = models.CharField(max_length=255)
	last_reading = models.DateTimeField()
	status = models.CharField(max_length=20, choices=STATUS_CHOICES) 
	owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
	category = models.ForeignKey(Category, on_delete=models.CASCADE)
	def __str__(self):
		return self.epc_id