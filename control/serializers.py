from .models import Asset, Owner, Category
from rest_framework import serializers

class OwnerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Owner
		fields = ('id', 'name', 'last_name',)

class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		fields = ('id', 'name',)

class AssetSerializer(serializers.ModelSerializer):
	class Meta:
		model = Asset
		fields = ('id', 'price', 'epc_id', 'last_reading', 'status', 'owner', 'category',)